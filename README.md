# MuEPL Mutation Tool

MuEPL is a mutation testing tool for the Event Processing Language (EPL) of EsperTech programming language. Given that the EPL of EsperTech is base on SQL, the tool includes SQL mutation operators and the specific mutation operators of the EPL of EsperTech. MuEPL is divided into four components: capturer, analyser, mutant generator and execution system. MuEPL is not subject to a framework testing so that you can use your own test suite.

## Installation

### Maven

It is necessary to download a Maven version higher than 3.0.3: http://maven.apache.org/download.html

Assuming the 3.0.3 version, the following steps should be done with root role:

Unix-based Operating Systems (Linux, Solaris and Mac OS X)

Extract the distribution archive, i.e. apache-maven-3.0.3-bin.tar.gz to the directory you wish to install Maven 3.0.3. These instructions assume you chose /usr/local/apache-maven. The subdirectory apache-maven-3.0.3 will be created from the archive.
In a command terminal, add the M2_HOME environment variable, e.g. 

<pre>
export M2_HOME=/usr/local/apache-maven/apache-maven-3.0.3
</pre>

Add the M2 environment variable, e.g. 

<pre>
export M2=$M2_HOME/bin
</pre>

Optional: Add the MAVEN_OPTS environment variable to specify JVM properties, e.g. 

<pre>
export MAVEN_OPTS="-Xms256m -Xmx512m"
</pre>

This environment variable can be used to supply extra options to Maven. Add M2 environment variable to your path, e.g. 

<pre>
export PATH=$M2:$PATH
</pre>

Make sure that JAVA_HOME is set to the location of your JDK, e.g. 

<pre>
export JAVA_HOME=/usr/java/jdk1.5.0_02
</pre> 

and that $JAVA_HOME/bin is in your PATH environment variable. Run `mvn --version` to verify that it is correctly installed.

h3. Esper dependences

Download the file pom.xml from the link https://gitlab.com/ucase/public/ws-bpel-testing-tools/tree/master/src/muepl, this file contains the dependences. In the working directory, you have to do `checkout` of /src/parent and /src/muepl. Once that our work directory is updated we have to type:

<pre>
mvn compile eclipse:eclipse
</pre>

This will download the dependences and will create an Eclipse project.

You have to define in Eclipse the variable `M2_REPO` with the following path: `~/.m2/repository` (From Eclipse: Window -> Preferences -> Java -> Build Path -> Classpath Variables).

To compile from Maven you can use `mvn compile`, and to test the code `mvn test`

## Options

To obtain information about the available options type:

<pre>
java -jar muepl.jar -help
</pre>

### Capture

The EPL of EsperTech is run under an Open Source Java CEP engine, so the queries can be integrated into the Java code. If that is the situation, the capture component has to be used. This component extracts the EPL queries and saves them into a file. If the queries are saved in a separate file and the Java program calls them while the execution, it is not necessary to use the capture component.

To run the capture component:

<pre>
capture outputEPLFolder "executioncommand" TestCaseFile
</pre>

outputEPLFolder is the folder where the file with the EPL queries will be saved, "executioncommand" is the order to execute the program, it must go quoted, and TestCaseFile is the test case file.

### Analyze

This component will indicate which mutation operators can be applied and their locations. To run the analyzer component:

<pre>
analyze PathToEPLFile [number_of_query_to_mutate]
</pre>

PathToEPLFile is the file with the EPL queries and after that, the number of queries to analyse has to be indicated.

### Applyall

This component will generate all the possible mutants. To run this component:

<pre>
applyall PathToOriginalFolder PathToEPLFile QueryLocationFolder PathToAnalyzeFile
</pre>

PathToOriginalFolder is the path of the original program, PathToEPLFile is the file with the EPL queries, QueryLocationFolder is the path (from the PathToOriginalFolder) where the EPL queries are saved and PathToAnalizeFile is the file with the output of the Analyze component.

### Runall

The execution of the test suite against the original and mutants. To run this component in a parallel way:

<pre>
runall PathToOriginalFolder PathToOutputsFile "executioncommand" TestCaseFile PathToFileWithMutantPaths
</pre>

If the programs cannot be executed in parallel:

<pre>
runallsequence PathToOriginalFolder PathToOutputsFile "executioncommand" TestCaseFile PathToFileWithMutantPaths
</pre>

PathToOriginalFolder is the path of the original program, PathToOutputsFile is the file where the outputs after the execution will be saved, "executioncommand" is the order to execute the program, it must go quoted, TestCaseFile is the test case file, and the PathToFileWithMutantsPaths is a file where the paths of the mutant programs are saved.

### Compare

This component does a comparision between the results of the test suite execution against the original program and the results for the mutants. To run the component:

<pre>
compare FileWithOutputPaths splitpart
</pre>

The FileWithOutputPaths is the file that contains the output of the previous component and splitpart is the string that will be used to apply the killing criteria.

This command displays a line per mutant, where for each test case prints:

1. “0” for the same result
2. “1” for a different result

The value “2” will be shown for invalid mutants.

If the times of the test suite execution have been measured, they will be displayed next to the results (a “T” is used to separate results and times).

Mutation Operators

This version of MuEPL includes the next mutation operators:

* Pattern Expression Mutation: PFP, PGR,PNR,POC,POM and PRE
* Windows Mutation: WLM, WTM, WBL and WBT
* Injection Attack Mutation: IWR and ICN
* Replacement: RAF, RAO, RBR, RGR, RJR, RLM, RLA, RAW, RBW, RLO, RNO, RNW, ROM, ROS, RRO, RRR_1, RRR_2, RSC, RSR_1, RSR_2, RSR_3 and RTU

## Contact

Report errors or ask for further information to Lorena Gutiérrez-Madroñal lorena.gutierrez@uca.es

_MuEPL has been legally registered in the "Registro Territorial de la propiedad intelectual de la Junta de Andalucía" with the code: CA-00033-2017_
